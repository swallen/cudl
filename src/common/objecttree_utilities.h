#ifndef OBJECTTREE_UTILITIES_H
#define OBJECTTREE_UTILITIES_H

using namespace std;

#include <string>
#include <iostream>
#include <fstream>

#include "block.h"
#include "param.h"

struct partListEntry_t {
	Block * libPart;
	int quantity;
};

struct refDesCount_t {
	string ref;
	int count;
};

void saveObjectTree(Block * root, string fileName);
Block * loadObjectTree(string fileName);

Block * flattenHierarchy(Block * root);
void flattenModule(Block * module);

void listParts(Block * root, list<string> * sortBy, list<partListEntry_t> * returnList);

void mergeNets(Net * dominant, Net * recessive);

void assignRefDes(Block * root, string refFile);
void assignRefDesRecursor(Block * root, ofstream * refFile, list<refDesCount_t> * tracker);

#endif