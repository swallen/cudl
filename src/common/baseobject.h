#ifndef BASEOBJECT_H
#define BASEOBJECT_H

using namespace std;

#include <string>
#include <list>
#include "save_load.h"
#include "codelocation.h"
#include "property.h"

class BaseObject {
public:
	//constructor
	BaseObject(string name, CodeLocation * loc);
	BaseObject(BinaryLoad * loader);
	
	//copy-constructor
	BaseObject(const BaseObject& from);
	
	//destructor
	virtual ~BaseObject();

	void addProperty(Property * prop);
	Property * getPropertyByName(string n);
	void addConstraint(Property * constr);
	Property * getConstraintByName(string n);
	void copyPropsConstrs(BaseObject * fromObj);
	void setName(string newName);
	string getName();
	CodeLocation * getLocation();
	void reportLocation();
	void saveToBinary(BinarySave * saver);
	virtual BaseObject * clone();
	void cleanup();
	
	list<Property*> properties;
	list<Property*> constraints;
	
private:
	string name;
	CodeLocation * loc;
};

#endif