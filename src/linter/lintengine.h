#ifndef LINTENGINE_H
#define LINTENGINE_H

using namespace std;

#include "block.h"

//******** LINT RULES ********//

//RULE - ERROR
//ports mark "supplied" must be connected to at least one port marked "supply"
//RULE - ERROR
//

class LintEngine {
public:
	LintEngine();
	
	void lintTree(Block * root, string logName);
	
	void reportWarning(string text, CodeLocation * loc);
	void reportError(string text, CodeLocation * loc);
private:
	ofstream logfile;
	int warnings = 0;
	int errors = 0;
};
#endif