#include "block.h"

Block::Block(string name, CodeLocation * definedLoc, bool part) : BaseObject(name, definedLoc) {
	part_flag = part;
}

Block::Block(BinaryLoad * loader) : BaseObject(loader) {
	alias = loader->loadString();
	
	part_flag = loader->loadBool();
	
	parent = NULL;
	
	//if the saved block has an instance location, load it
	if(loader->loadBool()) {
		instanceLoc = new CodeLocation(loader);
	}
	
	int numNets = loader->loadInt();
	for(int i=0; i<numNets; i++) {
		netList.push_back(new Net(loader));
	}
	
	int numPorts = loader->loadInt();
	for(int i=0; i<numPorts; i++) {
		Port * p = new Port(loader);
		if(!part_flag) {
			//reconnect down-net connections
			p->setDownNet(getNetByName(p->getName()));
		}
		addPort(p);
	}
	
	int numChildren = loader->loadInt();
	for(int i=0; i<numChildren; i++) {
		//load the block in
		Block * b = new Block(loader);
		
		//now we have to reconnect the block's upstream port connections. Load the net names in order.
		for(list<Port*>::iterator it=b->ports.begin(); it!=b->ports.end(); it++) {
			string upName = loader->loadString();
			if(!upName.empty()) {
				(*it)->setUpNet(getNetByName(upName));
			}
		}
		
		addChild(b);
	}
}

void Block::setAlias(string a) {
	alias = a;
}

string Block::getAlias() {
	return alias;
}

void Block::setInstanceLocation(CodeLocation * loc) {
	instanceLoc = loc;
}

CodeLocation * Block::getInstanceLocation() {
	return instanceLoc;
}

bool Block::isPart() { return part_flag; }

int Block::addNet(Net * n) {
	for(list<Net*>::iterator it = netList.begin(); it != netList.end(); it++) {
		if((*it)->getName().compare(n->getName()) == 0) {
			return -1;
		}
	}
	
	netList.push_back(n);
	return 0;
}

int Block::addChild(Block * b) {
	b->setParent(this);
	
	for(list<Block*>::iterator it = childBlocks.begin(); it != childBlocks.end(); it++) {
		if((*it)->getAlias().compare(b->getAlias()) == 0) {
			return -1;
		}
	}
	
	childBlocks.push_back(b);
	return 0;
}

void Block::setParent(Block * p) {
	parent = p;
}

Block * Block::getParent() {
	return parent;
}

Net * Block::getNetByName(string name) {
	//iterate through the netlist looking for a match
	for(list<Net*>::iterator it = netList.begin(); it != netList.end(); it++) {
		if((*it)->getName().compare(name) == 0) {
			return *it;
		}
	}
	
	return NULL;
}

void Block::addPort(Port * p) {
	p->setParent(this);
	ports.push_back(p);
}

Port * Block::getPortByName(string port_name) {
	for(list<Port*>::iterator it = ports.begin(); it != ports.end(); it++) {
		if((*it)->getName().compare(port_name) == 0) {
			return (*it);
		}
	}
	
	//we didn't find a port by the given name
	return NULL;
}

Block * Block::getChildByAlias(string child_alias) {
	for(list<Block*>::iterator it = childBlocks.begin(); it != childBlocks.end(); it++) {
		if((*it)->getAlias().compare(child_alias) == 0) {
			return (*it);
		}
	}
	
	//we didn't find a port by the given name
	return NULL;
}

void Block::saveToBinary(BinarySave * saver) {
	BaseObject::saveToBinary(saver);
	
	//print the alias
	saver->saveString(alias);
	
	//print the part flag
	saver->saveBool(part_flag);
	
	//print the instance location
	if(instanceLoc != NULL) {
		saver->saveBool(true);
		instanceLoc->saveToBinary(saver);
	} else saver->saveBool(false);
	
	//save the nets
	saver->saveInt(netList.size());
	for(list<Net*>::iterator it=netList.begin(); it!=netList.end(); it++) {
		(*it)->saveToBinary(saver);
	}
	
	//save the ports
	saver->saveInt(ports.size());
	for(list<Port*>::iterator it=ports.begin(); it!=ports.end(); it++) {
		(*it)->saveToBinary(saver);
	}
	
	
	//save the children
	saver->saveInt(childBlocks.size());
	for(list<Block*>::iterator it=childBlocks.begin(); it!=childBlocks.end(); it++) {
		(*it)->saveToBinary(saver);
		
		//print out the port upstream connections in order so we can reconnect them later
		for(list<Port*>::iterator it_p=(*it)->ports.begin(); it_p!=(*it)->ports.end(); it_p++) {
			Net * upNet = (*it_p)->getUpNet();
			if(upNet != NULL) {
				saver->saveString(upNet->getName());
			} else {
				saver->saveString("");
			}
		}
	}
}

Block * Block::clone() {
	return new Block(*this);
}

//copy constructor
Block::Block(const Block& from) : BaseObject(from) {
	alias = from.alias;
	if(from.instanceLoc != NULL) {
		instanceLoc = from.instanceLoc->clone();
	}
	
	part_flag = from.part_flag;
	
	//clone all of the nets
	for(list<Net*>::const_iterator it=from.netList.begin(); it!=from.netList.end(); it++) {
		netList.push_back((*it)->clone());
	}
	
	//clone all of the ports. This must be done AFTER cloning nets so the ports can reconnect to their net representation
	for(list<Port*>::const_iterator it=from.ports.begin(); it!=from.ports.end(); it++) {
		Port * p = (*it)->clone();
		
		if(!part_flag) {
			//reconnect down-net connections
			p->setDownNet(getNetByName(p->getName()));
		}
		
		addPort(p);
	}
	
	//clone all of the child blocks. This must be done AFTER cloning ports so children can reconnect to the nets in this block
	for(list<Block*>::const_iterator it=from.childBlocks.begin(); it!=from.childBlocks.end(); it++) {
		Block * b = (*it)->clone();
		b->setInstanceLocation((*it)->getInstanceLocation());
		
		//loop through the ports of the original child so we can get the names of the nets we're reconnecting to
		for(list<Port *>::iterator it_p=(*it)->ports.begin(); it_p!=(*it)->ports.end(); it_p++) {
			Net * oldUpNet = (*it_p)->getUpNet();
			
			if(oldUpNet != NULL) {
				//reconnect the new port to the new net with the same name as the old net connected to the old port
				b->getPortByName((*it_p)->getName())->setUpNet(getNetByName(oldUpNet->getName()));
			}
		}
		
		addChild(b);
	}
}

Block::~Block() {
	//this destructor deletes the entire object tree from this block down.
	//If you wish to save elements of the tree from destruction, remove them from
	//their parent's list before calling the parent destructor or they will be killed dead.
	
	//delete all of the children first
	list<Block *>::iterator it_b=childBlocks.begin();
	while(it_b!=childBlocks.end()) {
		delete (*it_b);
		it_b = childBlocks.erase(it_b);
	}
	
	//delete all of the nets
	list<Net *>::iterator it_n = netList.begin();
	while(it_n != netList.end()) {
		delete (*it_n);
		it_n = netList.erase(it_n);
	}
	
	//delete all of the ports
	list<Port *>::iterator it_p = ports.begin();
	while(it_p != ports.end()) {
		delete (*it_p);
		it_p = ports.erase(it_p);
	}
	
	if(instanceLoc != NULL)	delete instanceLoc;
}