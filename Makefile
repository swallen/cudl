all:
	+$(MAKE) -C src/common
	+$(MAKE) -C src/compiler
	+$(MAKE) -C src/export_kicad
	+$(MAKE) -C src/linter
