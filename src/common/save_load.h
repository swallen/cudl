#ifndef SAVE_LOAD_H
#define SAVE_LOAD_H

#include <iostream>
#include <string>
#include <fstream>
#include "param.h"

using namespace std;

#define START_SEQUENCE_SIZE 4

class BinaryLoad {
public:
	BinaryLoad(string fileName);
	~BinaryLoad();
	bool hasError();
	
	bool loadBool();
	uint8_t loadByte();
	string loadString();
	int loadInt();
	
private:
	ifstream rf;
};

class BinarySave {
public:
	BinarySave(string fileName);
	~BinarySave();
	bool hasError();
	
	void saveBool(bool b);
	void saveByte(uint8_t b);
	void saveString(string s);
	void saveInt(int i);
	
private:
	ofstream wf;
};

#endif