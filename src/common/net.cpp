#include "net.h"

Net::Net(string netName, CodeLocation * definedLoc) : BaseObject(netName, definedLoc){}
Net::Net(BinaryLoad * loader) : BaseObject(loader) {
	//like noted below, port<->net connections are remade at the block level. We do nothing more here.
}

void Net::addPortConnection(Port * p) {
	ports.push_back(p);
}

void Net::saveToBinary(BinarySave * saver) {
	BaseObject::saveToBinary(saver);
}

Net * Net::clone() {
	return new Net(*this);
}

Net::Net(const Net& from) : BaseObject(from) {
	//There aren't currently any net-specific things we need to clone here
	//ports are cloned in a block constext, so when a net is cloned it must be reconnected to its ports from a block context too
}

Net::~Net() {
	//we don't need to do anything here. We rely on the block destructors to delete all of the connected ports
}