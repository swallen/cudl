#include "lintengine.h"
#include "objecttree_utilities.h"

LintEngine::LintEngine() {}

void LintEngine::lintTree(Block * root, string logName) {
	logfile.open(logName);
	
	Block * flatRoot = flattenHierarchy(root);
	
	for(list<Net*>::iterator it_n=flatRoot->netList.begin(); it_n!=flatRoot->netList.end(); it_n++) {
		Net * n = *it_n;
		bool hasSupply = false;
		bool suppliesPort = false;
		
		for(list<Port*>::iterator it_p=n->ports.begin(); it_p!=n->ports.end(); it_p++) {
			Port * p = *it_p;
			cout << "Net \"" << n->getName() << "\" port \"" << p->getName() << "\"." << endl;
			
			Property * supply = p->getPropertyByName("supply");
			if(supply != NULL) {
				cout << p->getName() << endl;
				hasSupply = true;
			}
			Property * supplied = p->getPropertyByName("supplied");
			if(supplied != NULL) suppliesPort = true;
			
			for(list<Property*>::iterator it_c=p->constraints.begin(); it_c!=p->constraints.end(); it_c++) {
				Property * constr = *it_c;
				if(!constr->isChecked()) {
					reportWarning("Constraint \"" + constr->getName() + "\" has no corresponding check in linter.", constr->getLocation());
				}
			}
		}
		
		if(suppliesPort && (!hasSupply)) {
			reportError("Ports of net \"" + n->getName() + "\" are marked as \"supplied\", but net is not supplied.", n->getLocation());
		}
			
		for(list<Property*>::iterator it_c=n->constraints.begin(); it_c!=n->constraints.end(); it_c++) {
			Property * constr = *it_c;
			if(!constr->isChecked()) {
				reportWarning("Constraint \"" + constr->getName() + "\" has no corresponding check in linter.", constr->getLocation());
			}
		}
	}
	
	cout << "Lint found " << to_string(warnings) << " warnings and " << to_string(errors) << " errors." << endl;
	if((warnings > 0) || (errors > 0)) cout << "See \"" << logName << "\" for more details." << endl;
	
	logfile.close();
	
	delete flatRoot;
}

void LintEngine::reportWarning(string text, CodeLocation * loc) {
	logfile << "WARNING: " << text << endl;
	logfile << loc->getLocationString() << endl;
	cout << "WARNING: " << text << endl;
	loc->reportLocation();
	warnings++;
}

void LintEngine::reportError(string text, CodeLocation * loc) {
	logfile << "ERROR: " << text << endl;
	logfile << loc->getLocationString() << endl;
	cout << "ERROR: " << text << endl;
	loc->reportLocation();
	errors++;
}