#include "codelocation.h"

CodeLocation::CodeLocation(string filename, int line, int column) {
	file = filename;
	ln = line;
	col = column;
	
	hasLineCol_flag = true;
}

CodeLocation::CodeLocation(string reportString) {
	file = reportString;
	
	hasLineCol_flag = false;
}

CodeLocation::CodeLocation(BinaryLoad * loader) {
	file = loader->loadString();
	hasLineCol_flag = loader->loadBool();
	if(hasLineCol_flag) {
		ln = loader->loadInt();
		col = loader->loadInt();
	}
}

string CodeLocation::getFilename() {
	//return just the filename, removing path that may be present
#ifdef WINDOWS
	char sep = '\\';
#elif
	char sep = '/';
#endif

	size_t i = file.rfind(sep, file.length());
	if(i != string::npos) {
		return file.substr(i+1, file.length()-i);
	}
	
	return "";
}

string CodeLocation::getLibrary() {
	//the path may be relative, so we need to get the absolute path
	char * temp = &file[0];
	char resolvedPath[PATH_MAX];
	GetFullPathName(temp, PATH_MAX, resolvedPath, NULL);
	
	string sPath(resolvedPath);
	return sPath;
}

void CodeLocation::reportLocation() {
	if(hasLineCol_flag) {
		cout << file << ", line " << ln << ", column " << col << endl;
	} else {
		cout << file << endl;
	}
}

string CodeLocation::getLocationString() {
	return file + ", line " + to_string(ln) + ", column " + to_string(col);
}

void CodeLocation::saveToBinary(BinarySave * saver) {
	saver->saveString(file);
	saver->saveBool(hasLineCol_flag);
	if(hasLineCol_flag) {
		saver->saveInt(ln);
		saver->saveInt(col);
	}
}

CodeLocation * CodeLocation::clone() {
	return new CodeLocation(*this);
}

//copy constructor
CodeLocation::CodeLocation(const CodeLocation& from) {
	file = from.file;
	ln = from.ln;
	col = from.col;
	
	hasLineCol_flag = from.hasLineCol_flag;
}

CodeLocation::~CodeLocation() {
	//nothing to do here
}