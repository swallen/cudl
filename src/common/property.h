#ifndef PROPERTY_H
#define PROPERTY_H

using namespace std;

#include <string>
#include <vector>

#include "codelocation.h"
#include "save_load.h"

class Property {
public:
	Property(string propertyName, CodeLocation * definedLocation);
	Property(BinaryLoad * loader);
	void addArgument(string argument);
	void setArgument(int num, string argument);
	string getArgument(int num);
	int numArguments();
	
	string getName();
	void markChecked();
	bool isChecked();
	CodeLocation * getLocation();
	void reportLocation();
	void saveToBinary(BinarySave * saver);
	Property * clone();
private:
	string name;
	CodeLocation * loc;
	bool wasChecked = false;
	
	vector<string> arguments;
};

#endif