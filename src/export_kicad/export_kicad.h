#ifndef EXPORT_KICAD_H
#define EXPORT_KICAD_H

using namespace std;

#include <string>
#include <list>
#include <iostream>
#include <fstream>
#include <ctime>

#include "block.h"
#include "property.h"
#include "param.h"
#include "objecttree_utilities.h"

void export_kicad(Block * root, string fileName, string sourceFileName);

#endif