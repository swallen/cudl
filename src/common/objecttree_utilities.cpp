#include "objecttree_utilities.h"

void saveObjectTree(Block * root, string fileName) {
	BinarySave * saver = new BinarySave(fileName);
	if(saver->hasError()) {
		cout << "ERROR: could not open binary file \"" << fileName << "\"." << endl;
		return;
	}
	
	//this recursively loads all of the objects in the tree
	root->saveToBinary(saver);
	if(saver->hasError()) {
		cout << "ERROR: something happened while saving root." << endl;
	}
	
	delete saver;//Don't forget this! or the last bunch of bytes won't flush into the file
}

Block * loadObjectTree(string fileName) {
	BinaryLoad * loader = new BinaryLoad(fileName);
	if(loader->hasError()) {
		cout << "ERROR: could not open binary file \"" << fileName << "\"." << endl;
		return NULL;
	}
	
	Block * b = new Block(loader);
	if(loader->hasError()) {
		cout << "ERROR: something happened while loading root." << endl;
		return NULL;
	} else {
		return b;
	}
	
	delete loader;
}

Block * flattenHierarchy(Block * root) {
	
	Block * flatBlock = root->clone();
	
	flattenModule(flatBlock);
	
	return flatBlock;
}

void flattenModule(Block * module) {
	//first iterate through all of this module's children
	list<Block *>::iterator it=module->childBlocks.begin();
	list<Block *>::iterator it_end=module->childBlocks.end();
	while(it!=it_end) {
		Block * child = (*it);
		//check if the child block is a part
		if(!child->isPart()) {
			//if the block is a module, not a part, recurse into it.
			flattenModule(child);
			
			//next we need to pull all of the nets and parts of the child into this module
			
			//copy the parts in
			for(list<Block *>::iterator it_p=child->childBlocks.begin(); it_p!=child->childBlocks.end(); it_p++) {
				Block * childsPart = (*it_p);
				
				//we copy, not clone the pointer into this module's list
				module->childBlocks.push_back(childsPart);
			}
			
			//copy the nets in
			for(list<Net *>::iterator it_n=child->netList.begin(); it_n!=child->netList.end(); it_n++) {
				Net * childsNet = (*it_n);
				
				//first, we check to see how many upstream connections the net has
				bool has_upstream_connection = false;
				Net * upstreamNet = NULL;
				for(list<Port *>::iterator it_p=childsNet->ports.begin(); it_p!=childsNet->ports.end(); it_p++) {
					Port * p = (*it_p);
					
					//if the port is owned by the child (so can only go upstream) and the port is actually connected upstream
					if((p->getParent() == child) && (p->getUpNet() != NULL)) {
						//remove this port from the upstream and downstream net's list. The port will be gone soon
						p->getUpNet()->ports.remove(p);
						childsNet->ports.remove(p);
						
						//if we've already found an upstream net, the new upstream net should be merged with the old one
						if(has_upstream_connection) {
							mergeNets(upstreamNet, p->getUpNet());
						} else {
							//otherwise keep track of this net as the upstream net
							has_upstream_connection = true;
							upstreamNet = p->getUpNet();
						}
					}
				}
				
				if(has_upstream_connection) {
					//if the net has an upstream connection, we merge it into the upstream net
					mergeNets(upstreamNet, childsNet);
					
				} else {
					//if the net doesn't have an upstream connection, we copy it over
					//HOWEVER, if the module we're copying into has a net with the same name,
					//we need to prepend the alias of the child to the name of the net
					
					if(module->getNetByName(childsNet->getName())) {
						childsNet->setName(child->getAlias() + "." + childsNet->getName());
					}
					
					module->netList.push_back(childsNet);
				}
			}
			
			//we will now delete the child, but we need to clear some lists first
			//so objects we copied over aren't deleted by the child's destructor
			child->childBlocks.clear();
			child->netList.clear();
			
			it = module->childBlocks.erase(it);
			delete child;
		} else {
			++it;
		}
	}
}

void mergeNets(Net * dominant, Net * recessive) {
	for(list<Port *>::iterator it=recessive->ports.begin(); it!=recessive->ports.end(); it++) {
		Port * p = (*it);
		
		if(p->getDownNet() == recessive) {
			p->setDownNet(dominant);
		} else {
			p->setUpNet(dominant);
		}
	}
}

void listParts(Block * root, list<string> * sortBy, list<partListEntry_t> * returnList) {
	list<Block*>::iterator it_c;//this is for searching through the child blocks of other blocks
	list<partListEntry_t>::iterator it_l;//this is for searching through the part list we're building
	list<string>::iterator it_s;//this is for searching through the list of descriminators we're provided.
	
	//iterate through all the children of the block
	for(it_c=root->childBlocks.begin(); it_c!=root->childBlocks.end(); it_c++) {
		Block * child = (*it_c);
		
		if(child->isPart()) {
			//if the child is a part, we check to see if we have one matching the sorting requirements already in the list
			bool isMatch = false;
				
			for(it_l=returnList->begin(); it_l!=returnList->end(); it_l++) {
				isMatch = true;//every library component starts out hopeful
				Block * libPart = (*it_l).libPart;
				
				//for every element in the list, we check each discriminating value and see if it's the same as in the new block we found
				for(it_s=sortBy->begin(); it_s!=sortBy->end(); it_s++) {
					string key = (*it_s);
					
					//first check special keys
					if(key.compare("name") == 0) {
						if(libPart->getName().compare(child->getName()) != 0) {
							isMatch = false;
							break;
						}
						
						continue;
					}
					
					//we make sure both constraints and properties of the given key match
					
					Property * foundProp;
					Property * libProp;
					Property * foundConstr;
					Property * libConstr;
					
					//does the library part have anything by the given name?
					libProp = libPart->getPropertyByName(key);
					libConstr = libPart->getConstraintByName(key);
					
					//does the child have anything by the given name?
					foundProp = child->getPropertyByName(key);
					foundConstr = child->getConstraintByName(key);
					
					if( (foundProp == NULL) || (libProp == NULL) ) {
						if(foundProp != libProp) {
							isMatch = false;
							break;
						}
					//if both blocks have a property with the given key, we make sure the property has the same arguments
					} else {
						if(foundProp->numArguments() != libProp->numArguments()) {
							isMatch = false;
							break;
						}
						
						for(int i=0; i<foundProp->numArguments(); i++) {
							
							if(foundProp->getArgument(i).compare(libProp->getArgument(i)) != 0) {
								isMatch = false;
								break;
							}
						}
						
						if(!isMatch) break;
					}
					
					
					if( (foundConstr == NULL) || (libConstr == NULL) ) {
						if(foundConstr != libConstr) {
							isMatch = false;
							break;
						}
					//if both blocks have a constraint with the given key, we make sure the constraint has the same arguments.
					} else {
						if(foundConstr->numArguments() != libConstr->numArguments()) {
							isMatch = false;
							break;
						}
						
						for(int i=0; i<foundConstr->numArguments(); i++) {
							
							if(foundConstr->getArgument(i).compare(libConstr->getArgument(i)) != 0) {
								isMatch = false;
								break;
							}
						}
						
						if(!isMatch) break;
					}
				}//end iterating through sort keys
				
				//if we found a matching library component, give up looking through the library
				if(isMatch) break;
			}//end iterating though existing library parts
				
			//if we found a match somewhere in the library, increment the count on it
			if(isMatch) {
				(*it_l).quantity++;
				
			//otherwise, create a new entry in the library
			} else {
				returnList->push_back({child, 1});
			}
			
		//if the child isn't a part, recurse into it
		} else {
			listParts(child, sortBy, returnList);
		}
	}//end iterating through children of root
}


void assignRefDes(Block * root, string refFileName) {
	list<refDesCount_t> countTracker;
	
	ofstream refFile;
	refFile.open(refFileName);
	
	assignRefDesRecursor(root, &refFile, &countTracker);
	
	refFile.close();
}

void assignRefDesRecursor(Block * root, ofstream * refFile, list<refDesCount_t> * tracker) {
	for(list<Block *>::iterator it_c = root->childBlocks.begin(); it_c != root->childBlocks.end(); it_c++) {
		Block * child = (*it_c);
		
		if(child->isPart()) {
			string ref = "?";
			int count = 0;
			Property * prop = child->getPropertyByName("ref");
			
			//look for the "ref" property in the part. This is how we determine the refdes prefix
			//If "ref" isn't defined, we use the default "?" prefix
			if(prop != NULL) ref = prop->getArgument(0);
			
			bool foundMatch = false;
			for(list<refDesCount_t>::iterator it_t=tracker->begin(); it_t!=tracker->end(); it_t++) {
				if(ref.compare((*it_t).ref) == 0) {
					foundMatch = true;
					(*it_t).count++;
					count = (*it_t).count;
				}
			}
			
			//if we aren't already tracking this refdes, we add it to the track list.
			if(!foundMatch) {
				refDesCount_t newRefDes = {ref, 1};
				tracker->push_back(newRefDes);
				count = 1;
			}
			
			//now we need to create a string that explains how to get to this object
			string fullChildAlias = child->getAlias();
			Block * b = child->getParent();
			while(b->getParent() != NULL) {
				fullChildAlias = b->getAlias() + "." + fullChildAlias;
				b = b->getParent();
			}
			
			(*refFile) << fullChildAlias << "," << ref << to_string(count) << endl;
			Property * refDesProp = child->getPropertyByName("refdes");
			if(refDesProp == NULL) {
				refDesProp = new Property("refdes", NULL);
				refDesProp->addArgument(ref + to_string(count));
				child->addProperty(refDesProp);
			} else {
				refDesProp->setArgument(0, ref + to_string(count));
			}
		} else {
			assignRefDesRecursor(child, refFile, tracker);
		}
	}
}