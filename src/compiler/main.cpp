#include <vector>
#include <string>

using namespace std;

#include "block.h"
#include "fileparse.h"
#include "objecttree_utilities.h"

#define MIN_ARGUMENTS 1
#define MAX_ARGUMENTS 1
//arg	desc
//1		filename

string rootFileName;

int main(int argc, char *argv[])
{
	cout << "CuDL Compiler V0.1" << endl;
	
	//idiot check the arguments that have been passed in
	if(argc > (MAX_ARGUMENTS+1)) {
		cout << "Too many arguments" << endl;
		return 1;
	}
	
	if(argc < (MIN_ARGUMENTS+1)) {
		cout << "Too few arguments" << endl;
		return 1;
	}
	rootFileName = argv[1];
	
	FileParse rootParser(rootFileName, NULL);
	
	cout << "INFO: Compiling source files" << endl;
	rootParser.parseFile();
	
	if(rootParser.error_flag) {
		cout << "An error occured while compiling source." << endl;
		return 1;
	}
	
	Block * root = rootParser.blockLibrary.front();
	
	string binaryFileName = rootFileName.substr(0, rootFileName.size()-4) + "cudo";
	saveObjectTree(root, binaryFileName);
	
	cout << "CuDL Compiler complete." << endl;
	return 0;
}