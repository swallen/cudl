#include "port.h"

Port::Port(string portName, CodeLocation * definedLoc) : BaseObject(portName, definedLoc) {}

Port::Port(BinaryLoad * loader) : BaseObject(loader) {
	int numPins = loader->loadInt();
	
	for(int i=0; i<numPins; i++) {
		string pin = loader->loadString();
		addPin(pin);
	}
}

void Port::setParent(Block * b) {
	parent = b;
}

Block * Port::getParent() {
	return parent;
}

Net * Port::getUpNet() {return upNet;}
void Port::setUpNet(Net * n) {
	upNet = n;
	n->addPortConnection(this);
}

Net * Port::getDownNet() {return downNet;}
void Port::setDownNet(Net * n) {
	downNet = n;
	n->addPortConnection(this);
}

void Port::addPin(string pin) {
	pins.push_back(pin);
}

void Port::saveToBinary(BinarySave * saver) {
	BaseObject::saveToBinary(saver);
	
	saver->saveInt(pins.size());
	for(list<string>::iterator it=pins.begin(); it!=pins.end(); it++) {
		saver->saveString(*it);
	}
}

Port * Port::clone() {
	return new Port(*this);
}

Port::Port(const Port& from) : BaseObject(from) {
	//copy over the pin numbers
	for(list<string>::const_iterator it=from.pins.begin(); it!=from.pins.end(); it++) {
		addPin(*it);
	}
	//NET CONNECTIONS ARE BROKEN FOR THE CLONE AND MUST BE RE-ESTABLISHED
}

Port::~Port() {
	//Do nothing here. We rely on the block destructors to delete all of the connected nets.
}