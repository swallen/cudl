#ifndef FILEPARSE_H
#define FILEPARSE_H

using namespace std;

#include <sstream>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <list>
#include <vector>

#include "block.h"
#include "port.h"
#include "net.h"
#include "codelocation.h"
#include "property.h"

class FileParse {
public:
	list <Block*> blockLibrary;
	list <Block*> includedLibrary;
	list <BaseObject*> tagLibrary;
	list <FileParse*> childList;
	
	bool error_flag = false;
	
	struct symbol_t {
		int id;
		int line;
		int column;
	};
	
	FileParse(string name, FileParse * parent);
	~FileParse();
	void parseFile();
	Block * findBlockInLibrary(string name);
private:

	string fileName;
	ifstream file;
	
	FileParse * parentParser;

	int line=1;
	int symbol_line;
	int column=0;
	int symbol_column;
	
	list<symbol_t> symbols;
	
	typedef enum {
		RESERVED_IMPORT=0,
		RESERVED_MODULE=1,
		RESERVED_ENDMODULE=2,
		RESERVED_NET=3,
		RESERVED_PORT=4,
		RESERVED_PART=5,
		RESERVED_ENDPART=6,
		RESERVED_TAG=7,
		RESERVED_COMMA=8,
		RESERVED_LBRACKET=9,
		RESERVED_RBRACKET=10,
		RESERVED_LPAREN=11,
		RESERVED_RPAREN=12,
		RESERVED_LCURLY=13,
		RESERVED_RCURLY=14,
		RESERVED_SEMICOLON=15,
		RESERVED_COLON=16,
		RESERVED_NUMBER=17,
	} reserved_words_t;/* IMPORTANT! if you update this enum, update the list populator in the constructor! */
	
#define LAST_RESERVED_ID 17
	
	vector<string> symbolTable;

	typedef enum {
		READ,
		
		COMMENT_START,
		COMMENT_LINE,
		COMMENT_BLOCK,
		COMMENT_BLOCK_END,
		
		IMPORT_START,
		IMPORT_NAME,
		
		PARAMETRIC,
	} c_parse_state_t;

	int mainstate = READ;
	
	typedef enum {
		CONTEXT_NONE,
		CONTEXT_MODULE,
		CONTEXT_PART,
	} parse_context_t;
	
	int context = CONTEXT_NONE;
	
	typedef enum {
		MODPARSE_TYPE_NORMAL,
		MODPARSE_TYPE_MODULE,
		MODPARSE_TYPE_PARTPORT
	} modparse_type_t;
	
	string symbol;
	
	Block * activeBlock = NULL;
	
	int duplicate_count = 1;
	
	//PRIVATE METHODS
	void parseCharacter(char c);
	void saveSymbol();
	void parseSymbols();
	bool checkNewName(symbol_t sym);
	void reportDuplicateName(symbol_t sym);
	int parseModifiers(BaseObject * o, list<symbol_t>::iterator *it_p, int type);
	string getSymbolText(symbol_t sym);
	void reportUnexpectedSymbol(symbol_t sym);
	void reportSymbolLocation(symbol_t sym);
	void reportUnexpectedCharacter(char c);
	CodeLocation * createSymbolLocation(symbol_t sym);
	unsigned int symbolToUint(symbol_t sym);
};

#endif