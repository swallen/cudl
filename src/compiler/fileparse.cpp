#include "fileparse.h"

FileParse::FileParse(string name, FileParse * parent) {
	fileName = name;
	parentParser = parent;
	
	//This code structure isn't the cleanest, so the precompiler directive below helps keep things in line
#if LAST_RESERVED_ID != 17
#error "You need to update the reserved symbol table entries in the constructor"
#endif

	symbolTable.push_back("import");
	symbolTable.push_back("module");
	symbolTable.push_back("endmodule");
	symbolTable.push_back("net");
	symbolTable.push_back("port");
	symbolTable.push_back("part");
	symbolTable.push_back("endpart");
	symbolTable.push_back("tag");
	symbolTable.push_back(",");
	symbolTable.push_back("[");
	symbolTable.push_back("]");
	symbolTable.push_back("(");
	symbolTable.push_back(")");
	symbolTable.push_back("{");
	symbolTable.push_back("}");
	symbolTable.push_back(";");
	symbolTable.push_back(":");
	symbolTable.push_back("#");
	
	file.open(fileName, ios::in);
}

void FileParse::parseFile() {
	
	if(!file.is_open()) {
		cout << "ERROR: could not open file \"" << fileName << "\"." << endl;
		error_flag = true;
		return;
	}
	
	cout << "Compiling \"" << fileName << "\"." << endl;
	char character;
	while(file.get(character)) {
		
		parseCharacter(character);
		
		//if we encountered a problem above, we leave the loop early
		if(error_flag) {
			return;
		}
		
		//keep track of what line+character we're going to
		if(character == '\n') {
			line++;
			column=0;
		} else {
			column++;
		}
	}
	
	//if there is something in the symbol reader, save it now
	//this prevents symbols at the end of the file (with no spaces or newlines
	//after them) from being ignored
	saveSymbol();
	
	//now parse the symbols into an object tree
	parseSymbols();
	return;
}

void FileParse::parseCharacter(char c) {
	switch(mainstate) {
		
	case READ:
		switch(c) {
		//characters that simply mark the end of a symbol
		case ' ':
		case '\t':
		case '\n':
			saveSymbol();
			break;
			
		//characters that are a symbol only by themselves
		case ',':
		case '[':
		case ']':
		case '{':
		case '}':
		case '(':
		case ')':
		case ';':
		case ':':
		case '#':
			saveSymbol();
			symbol += c;
			symbol_line = line;
			symbol_column = column;
			saveSymbol();
			break;
			
		//characters that can be part of symbols
		case 'a': case 'b': case 'c': case 'd': case 'e': case 'f':
		case 'g': case 'h': case 'i': case 'j': case 'k': case 'l':
		case 'm': case 'n': case 'o': case 'p': case 'q': case 'r':
		case 's': case 't': case 'u': case 'v': case 'w': case 'x':
		case 'y': case 'z':
		case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
		case 'G': case 'H': case 'I': case 'J': case 'K': case 'L':
		case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R':
		case 'S': case 'T': case 'U': case 'V': case 'W': case 'X':
		case 'Y': case 'Z':
		case '_':
		case '.'://used for floating-point symbols. This technically allows '.' or '-' in any variable name, I might disallow that case eventually
		case '-'://
		case '0': case '1': case '2': case'3': case '4': case '5':
		case '6': case '7': case '8': case '9':
			if(symbol.empty()) {
				symbol_line = line;
				symbol_column = column;
			}
			symbol += c;
			break;
		//characters that represent comment starts
		case '/':
			saveSymbol();
			mainstate = COMMENT_START;
			break;
			
		//'<' and '>' surround parametric equations
		case '<':
			mainstate = PARAMETRIC;
			break;
			
		//any other characters are illegal in this state
		default:
			reportUnexpectedCharacter(c);
			break;
		}
		break;
		
	//comment flow control
	case COMMENT_START:
		switch(c) {
		case '/':
			mainstate = COMMENT_LINE;
			break;
		case '*':
			mainstate = COMMENT_BLOCK;
			break;
		default:
			reportUnexpectedCharacter(c);
			break;
		}
		break;
		
	case COMMENT_LINE:
		if(c == '\n') mainstate = READ;
		break;
		
	case COMMENT_BLOCK:
		if(c == '*') mainstate = COMMENT_BLOCK_END;
		break;
		
	case COMMENT_BLOCK_END:
		if(c == '/') mainstate = READ;
		else if(c != '*') mainstate = COMMENT_BLOCK;
		break;
		
	//In this case we're looking for the " chracter to denote the start of a path.
	//Only space, tab, or newline characters are allowed to separate an import from the path.
	case IMPORT_START:
		switch(c) {
		case ' ':
		case '\t':
		case '\n':
			break;
		case '\"':
			mainstate = IMPORT_NAME;
			symbol_line = line;
			symbol_column = column;
			break;
		default:
			reportUnexpectedCharacter(c);
			break;
		}
		break;
		
	case IMPORT_NAME:
		if(c != '"') symbol += c;
		else {
			saveSymbol();
			mainstate = READ;
		}
		
		break;
		
	case PARAMETRIC:
		if(c == '>') {
			symbol.clear();//TODO: parametrics
			mainstate = READ;
		} else {
			symbol += c;
		}
		break;
	}
}

void FileParse::saveSymbol() {
	if(!symbol.empty()) {
		int id = -1;
		//look and see if the symbol is already in the table
		for(int i=0; i<symbolTable.size(); i++) {
			if(symbolTable[i].compare(symbol) == 0) {
				id	= i;
				break;
			}
		}
		
		//if it wasn't found in the table, make a new entry
		if(id == -1) {
			id = symbolTable.size();
			symbolTable.push_back(symbol);
		}
		
		symbol_t sym_struct = {id, symbol_line, symbol_column};
		symbols.push_back(sym_struct);
		
		//cout << "Symbol: \"" << symbol << "\", line: " << symbol_line << ", column: " << symbol_column << endl;
		
		//the import symbol is followed by a symbol read with special rules
		if(id == RESERVED_IMPORT) {
			mainstate = IMPORT_START;
		}
		
		symbol.clear();
	}
}

void FileParse::parseSymbols() {
	//do a quick sanity check to see if the last symbol is a statement end (;). This saves us from 
	//having to check for the end every time we read in a symbol
	symbol_t sym_end = symbols.back();
	if(sym_end.id != RESERVED_SEMICOLON) {
		error_flag = true;
		cout << "Error: unexpected file end." << endl;
		reportSymbolLocation(sym_end);
		return;
	}
		
	
	for(list<symbol_t>::iterator it = symbols.begin(); it != symbols.end(); it++) {
		symbol_t sym_root = (*it);
		
		//we allow ';' to be a root symbol (aka an empty statement), but we don't do anything with it
		if(sym_root.id == RESERVED_SEMICOLON) continue;
		
		switch(context) {
		case CONTEXT_NONE:
			switch(sym_root.id) {
			case RESERVED_IMPORT:
			{
				//import statements make blocks from other files available to blocks within this file.
				symbol_t sym_path = (*(++it));
				if(sym_path.id != RESERVED_SEMICOLON) {
					//successful import declaration. Parse the file given
					FileParse * child;
					child = new FileParse(getSymbolText(sym_path), this);
					child->parseFile();
					if(!child->error_flag) {
						//copy over all of the blocks from the child parser
						for(list<Block *>::iterator it_b = child->blockLibrary.begin(); it_b != child->blockLibrary.end(); it_b++) {
							includedLibrary.push_back(*it_b);
						}
						
						childList.push_back(child);
						
						cout << "Returning to \"" << fileName << "\"." << endl;
					} else {
						error_flag = true;
						return;
					}
				} else {
					reportUnexpectedSymbol(sym_path);
					return;
				}
				
				break;
			}
			case RESERVED_MODULE:
			{
				//module statements represent a logical unit of a schematic. They can reference or be referenced by other blocks.
				//Blocks can represent an entire PCB
				
				//read the name and try to instantiate a new library block
				symbol_t sym_name = (*(++it));
				if(!checkNewName(sym_name)) return;
				
				CodeLocation * loc = new CodeLocation(fileName, sym_name.line, sym_name.column);
				activeBlock = new Block(getSymbolText(sym_name), loc, false);
				
				//read in modifiers
				if(parseModifiers((BaseObject *) activeBlock, &(++it), MODPARSE_TYPE_NORMAL)) return;
				
				context = CONTEXT_MODULE;
				
				//Move to the next root symbol
				break;
			}
			case RESERVED_PART:
			{
				//parts are blocks with a specialized syntax. They cannot have anything but ports, constraints, and properties. 
				//Parts are intended to represent finished components or boards
				
				//read the name and try to instantiate a new library block
				symbol_t sym_name = (*(++it));
				if(!checkNewName(sym_name)) return;
				
				CodeLocation * loc = new CodeLocation(fileName, sym_name.line, sym_name.column);
				activeBlock = new Block(getSymbolText(sym_name), loc, true);
				
				//read in modifiers
				if(parseModifiers((BaseObject *) activeBlock, &(++it), MODPARSE_TYPE_NORMAL)) return;
				
				context = CONTEXT_PART;
				
				//Move to the next root symbol
				break;
			}
			default:
				//Only imports, block declarations, or part declarations are allowed with no context
				reportUnexpectedSymbol(sym_root);
				return;
			}
			break;//CONTEXT_NONE
			
		case CONTEXT_MODULE:
			switch(sym_root.id) {
			case RESERVED_TAG:
			{
				symbol_t sym_name = (*(++it));
				BaseObject * tag = NULL;
				if(!checkNewName(sym_name)) return;
				
				CodeLocation * loc = new CodeLocation(fileName, sym_name.line, sym_name.column);
				tag = new BaseObject(getSymbolText(sym_name), loc);
				
				if(parseModifiers(tag, &(++it), MODPARSE_TYPE_NORMAL)) return;
				
				tagLibrary.push_back(tag);
				break;
			}
			case RESERVED_PORT:
			{
				symbol_t sym_name = (*(++it));
				if(!checkNewName(sym_name)) return;
				CodeLocation * loc = new CodeLocation(fileName, sym_name.line, sym_name.column);
				string sym_name_text = getSymbolText(sym_name);
				//first create a port object
				Port * p = new Port(sym_name_text, loc);
				if(parseModifiers((BaseObject *) p, &(++it), MODPARSE_TYPE_NORMAL)) return;
				activeBlock->addPort(p);
				
				//next create a net object and assign it to the port and the active block
				Net * n = new Net(sym_name_text, loc);
				p->setDownNet(n);
				activeBlock->addNet(n);
				break;
			}
			case RESERVED_NET:
			{
				symbol_t sym_name = (*(++it));
				if(!checkNewName(sym_name)) return;
				CodeLocation * loc = new CodeLocation(fileName, sym_name.line, sym_name.column);
				Net * n = new Net(getSymbolText(sym_name), loc);
				if(parseModifiers((BaseObject *) n, &(++it), MODPARSE_TYPE_NORMAL)) return;
				activeBlock->addNet(n);
				break;
			}
			case RESERVED_NUMBER:
			{
				//                    #6 #3 block;
				//catch invalid syntax --^
				if(duplicate_count != 1) {
					reportUnexpectedSymbol(sym_root);
					return;
				}
				
				symbol_t sym_count = (*(++it));
				duplicate_count = symbolToUint(sym_count);
				if(error_flag) return;
				
				if(duplicate_count == 0) {
					error_flag = true;
					cout << "ERROR: duplication count must be an integer number greater than zero." << endl;
					reportSymbolLocation(sym_count);
					return;
				}
				
				//skip this loop iteration, which makes the next symbol a root symbol.
				continue;
			}
			case RESERVED_ENDMODULE:
			{
				context = CONTEXT_NONE;
				if((++it)->id != RESERVED_SEMICOLON) {
					reportUnexpectedSymbol(*it);
					return;
				}
				
				blockLibrary.push_back(activeBlock);
				
				//clean up all the tag objects. They only hold block-level scope.
				for(list <BaseObject *>::iterator tag_it=tagLibrary.begin(); tag_it!=tagLibrary.end(); tag_it++) {
					delete (*tag_it);
				}
				tagLibrary.clear();
				
				activeBlock = NULL;
				break;
			}
			default:
			{
				//The only other symbols allowed in module contexts are user blocks. First check if the root is an illegal reserved symbol
				if(sym_root.id <= LAST_RESERVED_ID) {
					reportUnexpectedSymbol(sym_root);
					return;
				}
				
				//search block libraries for the symbol
				string sym_root_text = getSymbolText(sym_root);
				Block * libraryBlock = findBlockInLibrary(sym_root_text);
				if(libraryBlock == NULL) {//if we didn't find the block in the library
					cout << "ERROR: Symbol \"" << sym_root_text << "\" is not yet defined in this scope." << endl;
					reportSymbolLocation(sym_root);
					error_flag = true;
					return;
				}
				
				//check that the alias symbol is good
				symbol_t sym_alias = (*(++it));
				if(!checkNewName(sym_alias)) return;
				string sym_alias_text = getSymbolText(sym_alias);
				
				//clone the library block to a new location
				CodeLocation * loc = new CodeLocation(fileName, sym_root.line, sym_root.column);
				Block * instance = libraryBlock->clone();
				instance->setInstanceLocation(loc);
				
				//read in the modifiers for this instance if there are any
				if(parseModifiers((BaseObject *) instance, &(++it), MODPARSE_TYPE_MODULE)) return;
				
				if(duplicate_count == 1) {
					instance->setAlias(sym_alias_text);
					activeBlock->addChild(instance);
				} else {
					for(int i=0; i<duplicate_count; i++) {
						Block * dupInstance = instance->clone();
						dupInstance->setInstanceLocation(loc);
						dupInstance->setAlias(sym_alias_text + to_string(i));
						for(list<Port*>::iterator it_p=dupInstance->ports.begin(); it_p!=dupInstance->ports.end(); it_p++) {
							Port * originalPort = instance->getPortByName((*it_p)->getName());
							(*it_p)->setUpNet(originalPort->getUpNet());
						}
						activeBlock->addChild(dupInstance);
					}
				}
				
				//reset the count here so we don't trip the non-duplicateable check at the end of this loop
				duplicate_count = 1;
				
				break;
			}//default:
			}//switch(sym_root.id)
			break;//CONTEXT_MODULE
			
		//parts have a symplified syntax. They only allow the definition of tags and ports, and do not have nets or children.
		case CONTEXT_PART:
			switch(sym_root.id) {
			case RESERVED_TAG:
			{
				symbol_t sym_name = (*(++it));
				BaseObject * tag = NULL;
				if(!checkNewName(sym_name)) return;
				
				CodeLocation * loc = new CodeLocation(fileName, sym_name.line, sym_name.column);
				tag = new BaseObject(getSymbolText(sym_name), loc);
				
				if(parseModifiers(tag, &(++it), MODPARSE_TYPE_NORMAL)) return;
				
				tagLibrary.push_back(tag);
				break;
			}
			case RESERVED_ENDPART:
				context = CONTEXT_NONE;
				if((++it)->id != RESERVED_SEMICOLON) {
					reportUnexpectedSymbol(*it);
					return;
				}
				
				blockLibrary.push_back(activeBlock);
				
				//clean up all the tag objects. They only hold block-level scope.
				for(list <BaseObject *>::iterator tag_it=tagLibrary.begin(); tag_it!=tagLibrary.end(); tag_it++) {
					delete (*tag_it);
				}
				tagLibrary.clear();
				
				activeBlock = NULL;
				break;
			default:
				//if we aren't defining a tag or ending the part declaration, we must be defining a new port.
				//make sure it's not a reserved/used symbol
				if(!checkNewName(sym_root)) return;
				
				CodeLocation * loc = new CodeLocation(fileName, sym_root.line, sym_root.column);
				Port * p = new Port(getSymbolText(sym_root), loc);
				
				//read in any modifiers we're adding
				if(parseModifiers((BaseObject *) p, &(++it), MODPARSE_TYPE_PARTPORT)) return;
				
				//now read in the pins
				while(it->id != RESERVED_SEMICOLON) {
					//this symbol should be a pin number
					it++;
					if(it->id > LAST_RESERVED_ID) {
						p->addPin(getSymbolText(*it));
					} else {
						reportUnexpectedSymbol(*it);
						return;
					}
					
					it++;
					//the next symbol should be either a comma or a semicolon
					if((it->id != RESERVED_COMMA) && (it->id != RESERVED_SEMICOLON)) {
						reportUnexpectedSymbol(*it);
						return;
					}
				}
				
				activeBlock->addPort(p);
				break;
			}//switch(sym_root.id)
			break;//CONTEXT_PART
			
		default:
			error_flag = true;
			cout << "ERROR: compiler is in undefined context." << endl;
			return;
		}//switch(context)
		
		//make sure the duplicate count is still 1. If it is greater than 1, than that means the duplicate symbol was used before
		//a symbol type that does not handle duplication. This is an error case.
		if(duplicate_count != 1) {
			cout << "ERROR: symbol type does not support duplication operator. " << endl;
			reportSymbolLocation(sym_root);
			error_flag = true;
			return;
		}
		
	}//for(it=symbols.begin()
}

bool FileParse::checkNewName(symbol_t sym) {
	string sym_text = getSymbolText(sym);
	
	//first make sure the name isn't reserved
	if(sym.id <= LAST_RESERVED_ID) {
		error_flag = true;
		cout << "ERROR: reserved symbol " << sym_text << " cannot be used as an object name." << endl;
		reportSymbolLocation(sym);
		return false;
	}
	
	//make sure the name isn't already used
	switch(context) {
	case CONTEXT_MODULE:
	{
		//look for nets with the same name
		Net * foundNet = activeBlock->getNetByName(sym_text);
		if(foundNet != NULL) {
			reportDuplicateName(sym);
			foundNet->reportLocation();
			return false;
		}
		
		//look for instantiated blocks with the same alias
		Block * foundChild = activeBlock->getChildByAlias(sym_text);
		if(foundChild != NULL) {
			reportDuplicateName(sym);
			foundChild->reportLocation();
			return false;
		}
	}
			
		//INTENTIONAL SWITCH FALLTHROUGH
	case CONTEXT_PART:
	{
		//look for ports with the same name
		Port * foundPort = activeBlock->getPortByName(sym_text);
		if(foundPort != NULL) {
			reportDuplicateName(sym);
			foundPort->reportLocation();
			return false;
		}
	}
		
		//INTENTIONAL SWITCH FALLTHROUGH
	case CONTEXT_NONE:
	{
		//look for tags with the same name
		for(list<BaseObject*>::iterator it=tagLibrary.begin(); it!=tagLibrary.end(); it++) {
			if((*it)->getName().compare(sym_text) == 0) {
				reportDuplicateName(sym);
				(*it)->reportLocation();
				return false;
			}
		}
		
		//look for library blocks with the same name
		for(list<Block *>::iterator it = blockLibrary.begin(); it != blockLibrary.end(); it++) {
			if((*it)->getName().compare(sym_text) == 0) {
				reportDuplicateName(sym);
				(*it)->reportLocation();
				return false;
			}
		}
		break;
	}//case CONTEXT_NONE:
	}//switch(context)
	
	return true;
}

void FileParse::reportDuplicateName(symbol_t sym) {
	error_flag = true;
	cout << "ERROR: symbol name " << getSymbolText(sym) << " is already declared in the current scope." << endl;
	reportSymbolLocation(sym);
	cout << "First defined here:" << endl;
}

int FileParse::parseModifiers(BaseObject * o, list<symbol_t>::iterator *it_p, int type) {
	typedef enum {
		MOD_IDLE,
		MOD_CONSTR,
		MOD_CONSTR_ARG,
		MOD_PROP,
		MOD_PROP_ARG,
		MOD_PORT,
		MOD_PORT_ARG
	} parse_modifier_state_t;
	
	parse_modifier_state_t mod_parse_state = MOD_IDLE;
	
	Property * prop = NULL;
	Property * constr = NULL;
	Port * prt = NULL;
	
	Block * blk = NULL;
	if(type == MODPARSE_TYPE_MODULE) {
		blk = (Block *) o;
	}
	
	bool argument_parsed = false;
	
	int delimiter_id = (type == MODPARSE_TYPE_PARTPORT) ? RESERVED_COLON : RESERVED_SEMICOLON;
	
	while((*it_p)->id != delimiter_id) {
		symbol_t sym = (*(*it_p));
		string sym_text = getSymbolText(sym);
		switch(mod_parse_state) {
		case MOD_IDLE:
			switch(sym.id) {
			case RESERVED_LBRACKET:
				mod_parse_state = MOD_PROP;
				break;
			case RESERVED_LCURLY:
				mod_parse_state = MOD_CONSTR;
				break;
			case RESERVED_LPAREN:
				if(type == MODPARSE_TYPE_MODULE) {
					mod_parse_state = MOD_PORT;
				} else {
					reportUnexpectedSymbol(sym);
					return 1;
				}
				break;
			default:
				//the symbol might be a tag, and if so we pull the props and constraints out of it
				bool match_found = false;
				for(list<BaseObject *>::iterator it=tagLibrary.begin(); it != tagLibrary.end(); it++) {
					//check if the name matches
					if((*it)->getName().compare(sym_text) == 0) {
						//copy the properties and constraints from the tag object to the object we're parsing
						for(list<Property*>::iterator it_p=(*it)->properties.begin(); it_p!=(*it)->properties.end(); it_p++) {
							o->properties.push_back(*it_p);
						}
						for(list<Property*>::iterator it_c=(*it)->constraints.begin(); it_c!=(*it)->constraints.end(); it_c++) {
							o->constraints.push_back(*it_c);
						}
						match_found = true;
						break;
					}
				}
				if(match_found) break;
			
				reportUnexpectedSymbol(sym);
				return 1;
			}
			break;
			
		case MOD_CONSTR:
			//if we haven't started a constraint object yet, the only valid symbol is a new name
			if(constr == NULL) {
				//make sure it's a name and not a reserved symbol
				if(sym.id <= LAST_RESERVED_ID) {
					reportUnexpectedSymbol(sym);
					return 1;
				}
				constr = new Property(sym_text, new CodeLocation(fileName, sym.line, sym.column));
				
			} else {
				switch(sym.id) {
				//a '{' here means we are defining arguments for a constraint
				case RESERVED_LCURLY:
					mod_parse_state = MOD_CONSTR_ARG;
					break;
				//a ',' here means we are done defining this constraint and starting a new one
				case RESERVED_COMMA:
					o->addConstraint(constr);
					constr = NULL;
					break;
				//a '}' here means we are done defining constraints
				case RESERVED_RCURLY:
					mod_parse_state = MOD_IDLE;
					o->addConstraint(constr);
					constr = NULL;
					break;
				//anything else is not allowed
				default :
					reportUnexpectedSymbol(sym);
					return 1;
				}
			}
			break;//case MOD_CONSTR:
		
		case MOD_CONSTR_ARG:
			switch(sym.id) {
			//a '}' here denotes we are done adding arguments
			case RESERVED_RCURLY:
				mod_parse_state = MOD_CONSTR;
			//a ',' here denotes we are moving on to the next argument
			case RESERVED_COMMA:
				//make sure we actually parsed an argument before we allow more of a statement
				if(argument_parsed) {
					argument_parsed = false;
				} else {
					reportUnexpectedSymbol(sym);
					return 1;
				}
				break;
			//anything else should be a non-reserved name
			default:
				//reserved symbol check and make sure we didn't just see another name
				if((sym.id <= LAST_RESERVED_ID) || argument_parsed) {
					reportUnexpectedSymbol(sym);
					return 1;
				}
				
				constr->addArgument(sym_text);
				argument_parsed = true;
				break;
			}
			break;//case MOD_CONSTR_ARG:
		
		case MOD_PROP:
			//if we haven't started a property object yet, the only valid symbol is a new name
			if(prop == NULL) {
				//make sure it's a name and not a reserved symbol
				if(sym.id <= LAST_RESERVED_ID) {
					reportUnexpectedSymbol(sym);
					return 1;
				}
				prop = new Property(sym_text, new CodeLocation(fileName, sym.line, sym.column));
				
			} else {
				switch(sym.id) {
				//a '[' here means we are defining arguments for a property
				case RESERVED_LBRACKET:
					mod_parse_state = MOD_PROP_ARG;
					break;
				//a ',' here means we are done defining this property and starting a new one
				case RESERVED_COMMA:
					o->addProperty(prop);
					prop = NULL;
					break;
				//a ']' here means we are done defining properties
				case RESERVED_RBRACKET:
					mod_parse_state = MOD_IDLE;
					o->addProperty(prop);
					prop = NULL;
					break;
				//anything else is not allowed
				default :
					reportUnexpectedSymbol(sym);
					return 1;
				}
			}
			break;//case MOD_PROP:
		
		case MOD_PROP_ARG:
			switch(sym.id) {
			//a ']' here denotes we are done adding arguments
			case RESERVED_RBRACKET:
				mod_parse_state = MOD_PROP;
			//a ',' here denotes we are moving on to the next argument
			case RESERVED_COMMA:
				//make sure we actually parsed an argument before we allow more of a statement
				if(argument_parsed) {
					argument_parsed = false;
				} else {
					reportUnexpectedSymbol(sym);
					return 1;
				}
				break;
			//anything else should be a non-reserved name
			default:
				//reserved symbol check and make sure we didn't just see another name
				if((sym.id <= LAST_RESERVED_ID) || argument_parsed) {
					reportUnexpectedSymbol(sym);
					return 1;
				}
				
				prop->addArgument(sym_text);
				argument_parsed = true;
				break;
			}
			break;//case MOD_PROP_ARG:
		
		case MOD_PORT:
			//if we haven't started a port object yet, the only valid symbol is a port name
			if(prt == NULL) {
				//make sure it's a name and not a reserved symbol
				if(sym.id <= LAST_RESERVED_ID) {
					reportUnexpectedSymbol(sym);
					return 1;
				}
				prt = blk->getPortByName(sym_text);
				if(prt == NULL) {
					cout << "ERROR: block \"" << blk->getName() << "\" does not contain a port by the name \" << getSymbolText(sym) << \"." << endl;
					reportSymbolLocation(sym);
					return 1;
				}
				
			} else {
				switch(sym.id) {
				//a '(' here means we are defining the net to connect the port to
				case RESERVED_LPAREN:
					mod_parse_state = MOD_PORT_ARG;
					break;
				//a ')' here means we are done connecting ports
				case RESERVED_RPAREN:
					mod_parse_state = MOD_IDLE;
				//a ',' here means we are done connecting this port and moving on to the next one
				case RESERVED_COMMA:
					//make sure we connected a net before we move on
					if(argument_parsed) {
						argument_parsed = false;
					} else {
						cout << "ERROR: Expected net name." << endl;
						reportSymbolLocation(sym);
						return 1;
					}
					prt = NULL;
					break;
				//anything else is not allowed
				default :
					reportUnexpectedSymbol(sym);
					return 1;
				}
			}
			break;//case MOD_PORT:
		
		case MOD_PORT_ARG:
			switch(sym.id) {
			//a ')' here denotes we are done connecting this port
			case RESERVED_RPAREN:
				mod_parse_state = MOD_PORT;
				//make sure we actually parsed a net name before we are allowed to move on
				if(!argument_parsed) {
					reportUnexpectedSymbol(sym);
					return 1;
				}
				break;
			//anything else should be a non-reserved name
			default:
				//reserved symbol check and make sure we didn't just see another name
				if((sym.id <= LAST_RESERVED_ID) || argument_parsed) {
					reportUnexpectedSymbol(sym);
					return 1;
				}
				Net * n = activeBlock->getNetByName(sym_text);
				if(n == NULL) {
					cout << "ERROR: Active block \"" << activeBlock->getName() << "\" does not have a net named \"" << sym_text << "\"." << endl;
					reportSymbolLocation(sym);
					return 1;
				}
				prt->setUpNet(n);
				argument_parsed = true;
				break;
			}
			break;//case MOD_PORT_ARG:
		}
		
		//move to the next symbol
		(*it_p)++;
	}
	
	return 0;
}

string FileParse::getSymbolText(symbol_t sym) {
	return symbolTable[sym.id];
}

void FileParse::reportUnexpectedSymbol(symbol_t sym) {
	error_flag = true;
	cout << "Error: unexpected symbol found: " << getSymbolText(sym) << endl;
	reportSymbolLocation(sym);
}

void FileParse::reportSymbolLocation(symbol_t sym) {
	cout << fileName << " line: " << sym.line << ", column: " << sym.column << endl;
}

void FileParse::reportUnexpectedCharacter(char c) {
	error_flag = true;
	cout << "ERROR: unexpected character found: " << c << endl;
	cout << fileName << " line: " << line << ", column: " << column << endl;
}

CodeLocation * FileParse::createSymbolLocation(symbol_t sym) {
	return new CodeLocation(fileName, sym.line, sym.column);
}

Block * FileParse::findBlockInLibrary(string name) {
	//first look through the blocks we parsed ourself
	for(list<Block*>::iterator it = blockLibrary.begin(); it != blockLibrary.end(); it++) {
		if((*it)->getName().compare(name) == 0) {
			return *it;
		}
	}
	
	//next, search the blocks we included from our children
	for(list<Block*>::iterator it=includedLibrary.begin(); it!=includedLibrary.end(); it++) {
		if((*it)->getName().compare(name) == 0) {
			return *it;
		}
	}
	
	//next, if we have a parent look through the parent's library (this recurses to their parent, and so on).
	if(parentParser != NULL) {
		return parentParser->findBlockInLibrary(name);
	}
	
	//if we don't have a parent, return null
	return NULL;
	//this function will recursively look all the way up the file parser tree.
	//The effect is that any block defined in a file is in scope for any additional
	//files down the hierarchy. So if you want a globally defined block, include it
	//or define it at the top level file.
	
	//if the block is not found at the top level, the top level returns null. The
	//null is then dumped all the way down to the originating FileParse object
}

FileParse::~FileParse() {
	file.close();
	
	//remove all of the library blocks
	for(list<Block *>::iterator it=blockLibrary.begin(); it!=blockLibrary.end(); it++) {
		delete (*it);
	}
	blockLibrary.clear();
	
	//delete all of the child parsers. This will call their destructors, which will kill their children, and so on.
	for(list<FileParse*>::iterator it=childList.begin(); it!=childList.end(); it++) {
		delete (*it);
	}
	childList.clear();
	
	parentParser = NULL;
	
	symbols.clear();
	symbolTable.clear();
}

unsigned int FileParse::symbolToUint(symbol_t sym) {
	unsigned int val = 0;
	string sym_text = getSymbolText(sym);
	for(auto c : sym_text) {
		val *= 10;
		if(c >= 48 && c <= 57) {
			val = c - 48;
		} else {
			cout << "ERROR: expected positive integer here. Instead got \"" << sym_text << "\"." << endl;
			reportSymbolLocation(sym);
			error_flag = true;
			return 0;
		}
	}
	
	return val;
}