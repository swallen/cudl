#ifndef CODELOCATION_H
#define CODELOCATION_H

using namespace std;

#include <string>
#include <iostream>

#include "param.h"
#include "save_load.h"

#ifdef WINDOWS
#include <windows.h>
#endif

class CodeLocation {
public:
	CodeLocation(string filename, int line, int column);
	CodeLocation(string reportString);
	CodeLocation(BinaryLoad * loader);
	CodeLocation(const CodeLocation& from);
	~CodeLocation();
	
	string getFilename();
	string getLibrary();
	
	void reportLocation();
	string getLocationString();
	
	void saveToBinary(BinarySave * saver);
	
	CodeLocation * clone();
	
	string file;
	int ln;
	int col;
	
	bool hasLineCol_flag;
};

#endif