#include "save_load.h"

//FILE LOAD CLASS

BinaryLoad::BinaryLoad(string fileName) {
	rf.open(fileName, ios::in | ios::binary);
	
	if(hasError()) return;
	
	//scan forward until we find the start of stream sequence
	uint8_t bytes[START_SEQUENCE_SIZE] = { 0 };
	while(!hasError()) {
		for(int i=START_SEQUENCE_SIZE-2; i>=0; i--) {
			bytes[i+1] = bytes[i];
		}
		
		bytes[0] = loadByte();
		
		//check if the bytes match
		if( (bytes[3] == 0x01) &&
			(bytes[2] == 0x02) &&
			(bytes[1] == 0x03) &&
			(bytes[0] == 0x04))
			{
			break;	
		}
	}
}
BinaryLoad::~BinaryLoad() {
	rf.close();
}

bool BinaryLoad::hasError() {
	if(!rf) {
		return true;
	}
	
	return false;
}

bool BinaryLoad::loadBool() {
	return (loadByte() == 0x01) ? true : false;
}

uint8_t BinaryLoad::loadByte() {
	uint8_t b;
	rf.read((char *) &b, 1);
	return b;
}

string BinaryLoad::loadString() {
	string s;
	size_t size;
	rf.read((char *) &size, sizeof(size_t));
	s.resize(size);
	rf.read((char *) s.c_str(), size);
	return s;
}

int BinaryLoad::loadInt() {
	int i;
	rf.read((char *) &i, sizeof(int));
	return i;
}

//FILE SAVE CLASS

BinarySave::BinarySave(string fileName) {
	wf.open(fileName, ios::out | ios::binary);
	
	if(hasError()) return;
	
	//print a plaintext header string
	string headerString = "CuDL Compiler V" + to_string(CUDL_VERSION_MAJOR) + "." + to_string(CUDL_VERSION_MINOR) + " object binary file.";
	saveString(headerString);
	
	//print a start of stream sequence
	uint8_t start[4] = {0x01, 0x02, 0x03, 0x04};
	wf.write((char *) start, 4);
}
BinarySave::~BinarySave() {
	wf.close();
}

bool BinarySave::hasError() {
	if(!wf) {
		return true;
	}
	
	return false;
}

void BinarySave::saveBool(bool b) {
	if(b) {
		saveByte(0x01);
	} else {
		saveByte(0x00);
	}
}

void BinarySave::saveByte(uint8_t b) {
	wf.write((char *) &b, 1);
}

void BinarySave::saveString(string s) {
	size_t size = s.size();
	wf.write((char *) &size, sizeof(size));
	wf.write((char *) s.c_str(), size);
}

void BinarySave::saveInt(int i) {
	wf.write((char *) &i, sizeof(int));
	if(hasError()) cout << "ERROR saving int!" << endl;
}