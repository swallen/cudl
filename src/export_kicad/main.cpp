#include "export_kicad.h"
#include "block.h"
#include "objecttree_utilities.h"

#define MIN_ARGUMENTS 1
#define MAX_ARGUMENTS 1
//arg	desc
//1		filename

int main(int argc, char *argv[]) {
	cout << "CuDL Kicad Exporter" << endl;
	
	//idiot check the arguments that have been passed in
	if(argc > (MAX_ARGUMENTS+1)) {
		cout << "Too many arguments" << endl;
		return 1;
	}
	
	if(argc < (MIN_ARGUMENTS+1)) {
		cout << "Too few arguments" << endl;
		return 1;
	}
	string sourceFileName = argv[1];
	
	cout << "Loading from file \"" << sourceFileName << "\"." << endl;
	
	Block * root = loadObjectTree(sourceFileName);
	
	//assign refdes to all of the parts
	assignRefDes(root, "refdes_mapping.csv");
	
	//Flatten the object tree hierarchy to make it easier to parse.
	Block * flatRoot = flattenHierarchy(root);
	delete root;
	
	string outFileName = sourceFileName.substr(0, sourceFileName.size()-4) + "net";
	
	cout << "Creating netlist \"" << outFileName << "\"." << endl;
	
	//Now we print all of the data to the output file
	ofstream outfile;
	outfile.open(outFileName);
	
	outfile << "(export (version D)" << endl;
	outfile << "  (design" << endl;
	outfile << "    (source " << sourceFileName << ")" << endl;
	
	time_t t = time(0);
	tm* now = localtime(&t);
	outfile << "    (date \"" << (now->tm_mon+1) << "/" << now->tm_mday << "/" << (now->tm_year + 1900)
							<< " " << now->tm_hour << ":" << now->tm_min << ":" << now->tm_sec << "\")" << endl;
	outfile << "    (tool \"CuDL compiler V" << CUDL_VERSION_MAJOR << "." << CUDL_VERSION_MINOR << "\")" << endl;
	outfile << "  )" << endl;//end of design
	
	outfile << "  (components" << endl;
	
	for(list<Block *>::iterator it=flatRoot->childBlocks.begin(); it!=flatRoot->childBlocks.end(); it++) {
		Block * part = (*it);
		
	outfile << "    (comp (ref " << part->getPropertyByName("refdes")->getArgument(0) << ")" << endl;
	
	outfile << "      (value " << part->getName() << ")" << endl;//TODO support for printing a "value" property here if the part has one
		Property * footprint = part->getPropertyByName("footprint");
		if(footprint != NULL) {
	outfile << "      (footprint " << footprint->getArgument(0) << ":" << footprint->getArgument(1) << ")" << endl;
		} else {
			cout << "WARNING: part \"" << part->getName() << "\" does not have a footprint defined." << endl;
			part->reportLocation();
		}
		
		//cout <<"Filename: " << part->getLocation()->getFilename() << endl;
		
	outfile << "      (libsource (lib " << part->getLocation()->getFilename() << 
							") (part " << part->getName() << ") (description \"none\"))" << endl;
							
		//now we need to create a string that explains how to get to this object
		string path = "/" + part->getAlias();
		Block * b = part->getParent();
		while(b->getParent() != NULL) {
			path = "/" + b->getAlias() + path;
			b = b->getParent();
		}
	outfile << "      (sheetpath (names " << path << ") (tstamps " << path << "))" << endl;
	outfile << "    )" << endl;//end of an individual component
	}
	
	outfile << "  )" << endl;//end of components
	/*
	outfile << "  (libparts" << endl;
	
	//next we use the listParts utility to build a list of library parts for printing in the libparts section
	//build the list of keys we want to sort by
	list<string> sortBy;
	sortBy.push_back("name");
	sortBy.push_back("footprint");
	
	//perform the sort
	list<partListEntry_t> partList;
	listParts(root, &sortBy, &partList);
	
	for(list<partListEntry_t>::iterator it=partList.begin(); it!=partList.end(); it++) {
	outfile << "    (libpart (lib " << it->libPart->getLocation()->getFilename() <<
						") (part " << it->libPart->getName() << ")" << endl;
						
	outfile << "      (fields" << endl;
		for(list<Property*>::iterator it_p=it->libPart->properties.begin(); it_p!=it->libPart->properties.end(); it_p++) {
			
			//we will only print ref or footprint properties here. So make sure we have one of those
			string propName = (*it_p)->getName();
			if(propName.compare("ref") == 0 || propName.compare("footprint") == 0) {		
	outfile << "        (field (name " << propName << ") " << (*it_p)->getArgument(0) << ")" << endl;
			}
			
		}
	outfile << "      )" << endl;
	
	outfile << "      (pins" << endl;
		for(list<Port *>::iterator it_p=it->libPart->ports.begin(); it_p!=it->libPart->ports.end(); it_p++) {
			for(list<string>::iterator it_s=(*it_p)->pins.begin(); it_s!=(*it_p)->pins.end(); it_s++) {
	outfile << "        (pin (num " << (*it_s) << ") (name " << (*it_p)->getName() << "))" << endl;
			}
		}
						
	outfile << "    )" << endl;
	}
	
	outfile << "  )" << endl;//end of libparts
	
	
	
	outfile << "  (libraries" << endl;
	
	list<string> libraries;
	//look through our library parts list for their library sources
	for(list<partListEntry_t>::iterator it=partList.begin(); it!=partList.end(); it++) {
		string partsLibrary = (*it).libPart->getLocation()->getLibrary();
		bool foundMatch = false;
		
		//iterate through the libraries we already found so there's no duplicates
		for(list<string>::iterator it_s=libraries.begin(); it_s!=libraries.end(); it_s++) {
			if(partsLibrary.compare(*it_s) == 0) {
				foundMatch = true;
				break;
			}
		}
		
		if(foundMatch) continue;
		
		libraries.push_back(partsLibrary);
	outfile << "    (library (logical " << (*it).libPart->getLocation()->getFilename() << ") (uri \"" << partsLibrary << "\"))" << endl;
	}
	
	outfile << "  )" << endl;//end of libraries
	
	*/
	
	
	outfile << "  (nets" << endl;
	
	//iterate through all of the nets in the flattened design
	int netCode = 1;
	for(list<Net*>::iterator it_n=flatRoot->netList.begin(); it_n!=flatRoot->netList.end(); it_n++) {
		Net * n = (*it_n);
		
	outfile << "    (net (code " << netCode << ") (name " << n->getName() << ")" << endl;
		netCode++;
		
		//iterate through every port connected to the net
		for(list<Port*>::iterator it_p=n->ports.begin(); it_p!=n->ports.end(); it_p++) {
			Port * p = (*it_p);
			
			//iterate through every pin that the port represents
			for(list<string>::iterator it_s=p->pins.begin(); it_s!=p->pins.end(); it_s++) {
				//TODO: getParent()->getName() needs to be replaced with getParent()->getRefDes() once ref des are implemented
	outfile << "      (node (ref " << p->getParent()->getPropertyByName("refdes")->getArgument(0) << ") (pin " << (*it_s) << "))" << endl;
			}
		}
	
	outfile << "    )" << endl;//end of a net section
	}
	
	outfile << "  )" << endl;//end of nets
	
	outfile << ")" << endl;//end of export
	
	outfile.close();
	
	//we're done with the flattened root block now. Free the memory
	delete flatRoot;
	
	cout << "File export complete." << endl;
}