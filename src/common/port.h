#ifndef PORT_H
#define PORT_H

using namespace std;

#include <string>
#include <list>

#include "baseobject.h"
#include "net.h"
#include "block.h"
#include "save_load.h"

class Block;
class Net;

class Port: public BaseObject {
public:
	//PUBLIC METHODS
	Port(string portName, CodeLocation * definedLoc);
	Port(BinaryLoad * loader);
	Port(const Port& from);
	~Port();
	
	void addPin(string pin);
	void setUpNet(Net * n);
	void setDownNet(Net * n);
	
	Net * getUpNet();
	Net * getDownNet();
	
	void setParent(Block * b);
	Block * getParent();
	
	void saveToBinary(BinarySave * saver);
	
	virtual Port * clone();
	
	list<string> pins;
private:
	Block * parent;
	Net * upNet = NULL;
	Net * downNet = NULL;
};//class Net

#endif