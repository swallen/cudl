#ifndef NET_H
#define NET_H

using namespace std;

#include <string>
#include <list>

#include "baseobject.h"
#include "port.h"
#include "save_load.h"

class Port;

class Net: public BaseObject {
public:
	//PUBLIC METHODS
	Net(string netName, CodeLocation * definedLoc);
	Net(BinaryLoad * loader);
	Net(const Net& from);
	~Net();
	
	void addPortConnection(Port * p);
	
	void saveToBinary(BinarySave * saver);
	virtual Net * clone();
	
	
	list<Port*> ports;
private:
};//class Net

#endif