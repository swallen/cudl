#ifndef BLOCK_H
#define BLOCK_H

using namespace std;

#include <string>
#include <list>

#include "baseobject.h"
#include "net.h"
#include "port.h"
#include "save_load.h"

class Net;
class Port;

class Block: public BaseObject {
public:
	//PUBLIC METHODS
	Block(string name, CodeLocation * definedLoc, bool part);
	Block(BinaryLoad * loader);
	
	//copy constructor
	Block(const Block& from);
	
	~Block();
	
	void setAlias(string a);
	string getAlias();
	void setInstanceLocation(CodeLocation * loc);
	CodeLocation * getInstanceLocation();
	
	bool isPart();
	
	int addNet(Net * n);
	int addChild(Block * b);
	
	void setParent(Block * p);
	Block * getParent();
	
	Net * getNetByName(string name);
	Port * getPortByName(string port_name);
	Block * getChildByAlias(string child_alias);
	
	void addPort(Port * p);
	
	void saveToBinary(BinarySave * saver);
	
	virtual Block * clone();
	void cleanup();
	
	list<Net*> netList;
	list<Block*> childBlocks;
	list<Port*> ports;
	
private:
	string alias;
	bool part_flag;
	
	CodeLocation * instanceLoc = NULL;
	
	Block * parent;
	
	//PRIVATE METHODS
};//class Block

#endif