#include "baseobject.h"

BaseObject::BaseObject(string objectName, CodeLocation * location) {
	name = objectName;
	loc = location;
}

BaseObject::BaseObject(BinaryLoad * loader) {
	name = loader->loadString();
	loc = new CodeLocation(loader);
	
	int props_size = loader->loadInt();
	for(int i=0; i<props_size; i++) {
		addProperty(new Property(loader));
	}
	
	int constrs_size = loader->loadInt();
	for(int i=0; i<constrs_size; i++) {
		addConstraint(new Property(loader));
	}
}

void BaseObject::addProperty(Property * prop) {
	properties.push_back(prop);
}

Property * BaseObject::getPropertyByName(string n) {
	for(list<Property*>::iterator it=properties.begin(); it!=properties.end(); it++) {
		if((*it)->getName().compare(n) == 0) return (*it);
	}
	
	return NULL;
}

void BaseObject::addConstraint(Property * constr) {
	constraints.push_back(constr);
}

Property * BaseObject::getConstraintByName(string n) {
	for(list<Property*>::iterator it=constraints.begin(); it!=constraints.end(); it++) {
		if((*it)->getName().compare(n) == 0) return (*it);
	}
	
	return NULL;
}

void BaseObject::copyPropsConstrs(BaseObject * fromObj) {
	//copy the properties over
	for(list<Property*>::iterator it=fromObj->properties.begin(); it!=fromObj->properties.end(); it++) {
		properties.push_back(*it);
	}
	
	//copy the constraints over
	for(list<Property*>::iterator it=fromObj->constraints.begin(); it!=fromObj->constraints.end(); it++) {
		constraints.push_back(*it);
	}
}

void BaseObject::setName(string newName) {
	name = newName;
}

string BaseObject::getName() {
	return name;
}

CodeLocation * BaseObject::getLocation() {
	return loc;
}

void BaseObject::reportLocation() {
	loc->reportLocation();
}

void BaseObject::saveToBinary(BinarySave * saver) {
	//print the name
	saver->saveString(name);
	
	//print the location
	loc->saveToBinary(saver);
	
	//print the properties
	saver->saveInt(properties.size());
	for(list<Property*>::iterator it=properties.begin(); it!=properties.end(); it++) {
		(*it)->saveToBinary(saver);
	}
	
	//print the constraints
	saver->saveInt(constraints.size());
	for(list<Property*>::iterator it=constraints.begin(); it!=constraints.end(); it++) {
		(*it)->saveToBinary(saver);
	}
}

BaseObject * BaseObject::clone() {
	return new BaseObject(*this);
}

//copy constructor
BaseObject::BaseObject(const BaseObject& from) {
	name = from.name;
	loc = from.loc->clone();
	
	//copy over all constraints
	for(list<Property*>::const_iterator it=from.constraints.begin(); it!=from.constraints.end(); it++) {
		Property* con = (*it)->clone();
		constraints.push_back(con);
	}
	
	//copy over all properties
	for(list<Property*>::const_iterator it=from.properties.begin(); it!=from.properties.end(); it++) {
		Property * prop = (*it)->clone();
		properties.push_back(prop);
	}
}

BaseObject::~BaseObject() {
	//delete all constraints
	list<Property*>::iterator it_c = constraints.begin();
	while(it_c!=constraints.end()) {
		delete *it_c;
		it_c = constraints.erase(it_c);
	}
	
	//delete all properties
	list<Property*>::iterator it_p = properties.begin();
	while(it_p!=properties.end()) {
		delete *it_p;
		it_p = properties.erase(it_p);
	}
	
	//delete the location object
	delete loc;
}