#include "property.h"

Property::Property(string propertyName, CodeLocation * definedLocation) {
	name = propertyName;
	loc = definedLocation;
}

Property::Property(BinaryLoad * loader) {
	name = loader->loadString();
	loc = new CodeLocation(loader);
	int arg_size = loader->loadInt();
	for(int i=0; i<arg_size; i++) {
		string arg = loader->loadString();
		addArgument(arg);
	}
}

string Property::getName() {
	return name;
}

void Property::addArgument(string argument) {
	arguments.push_back(argument);
}

void Property::setArgument(int num, string argument) {
	if(num < arguments.size()) {
		arguments[num] = argument;
	}
}

string Property::getArgument(int num) {
	if(num < arguments.size()) {
		return arguments[num];
	} else {
		return "";
	}
}

int Property::numArguments() {
	return arguments.size();
}

void Property::markChecked() {
	wasChecked = true;
}

bool Property::isChecked() {
	return wasChecked;
}

CodeLocation * Property::getLocation() {
	return loc;
}

void Property::reportLocation() {
	loc->reportLocation();
}

void Property::saveToBinary(BinarySave * saver) {
	saver->saveString(name);
	loc->saveToBinary(saver);
	saver->saveInt(arguments.size());
	for(vector<string>::iterator it=arguments.begin(); it != arguments.end(); it++) {
		saver->saveString(*it);
	}
}

Property * Property::clone() {
	CodeLocation * newLoc = NULL;
	if(loc != NULL) {
		newLoc = loc->clone();
	}
	
	Property * prop = new Property(name, newLoc);
	for(vector<string>::iterator it=arguments.begin(); it!=arguments.end(); it++) {
		prop->addArgument(*it);
	}
	
	return prop;
}