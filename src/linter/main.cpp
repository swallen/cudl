#include "block.h"
#include "objecttree_utilities.h"
#include "lintengine.h"

#define MIN_ARGUMENTS 1
#define MAX_ARGUMENTS 1
//arg	desc
//1		filename

int main(int argc, char *argv[]) {
	cout << "CuDL Linter." << endl;
	
	//idiot check the arguments that have been passed in
	if(argc > (MAX_ARGUMENTS+1)) {
		cout << "Too many arguments" << endl;
		return 1;
	}
	
	if(argc < (MIN_ARGUMENTS+1)) {
		cout << "Too few arguments" << endl;
		return 1;
	}
	string sourceFileName = argv[1];
	
	cout << "Loading object tree from file \"" << sourceFileName << "\"." << endl;
	
	Block * root = loadObjectTree(sourceFileName);
	
	//cout << "Tree loaded." << endl;
	
	LintEngine * engine = new LintEngine();
	
	engine->lintTree(root, "lint_log.txt");
	
	delete engine;
	delete root;
	cout << "Lint complete." << endl;
}